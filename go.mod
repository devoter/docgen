module gitlab.com/devoter/docgen

go 1.20

require (
	github.com/tidwall/jsonc v0.3.2
	gopkg.in/yaml.v3 v3.0.1
)
