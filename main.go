package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"

	"gopkg.in/yaml.v3"

	"gitlab.com/devoter/docgen/parsers"
)

var Version = "undefined"

func main() {
	basePath := flag.String("base", "base.yml", "base data filename")
	schemasStr := flag.String("schemas", "", "schemas files (separated by comma)")
	securitySchemesStr := flag.String("security", "", "security schemes (separated by comma)")
	pathsStr := flag.String("paths", "", "paths files (separated by comma)")
	tagsStr := flag.String("tags", "", "tags files (separated by comma")
	responsesStr := flag.String("responses", "", "responses files (separated by comma)")
	customStr := flag.String("custom", "", "custom sections (separated by comma, name and type prefixes are required")
	output := flag.String("output", "out/api.yml", "output filename")
	toStdout := flag.Bool("stdout", false, "output to STDOUT")
	outputJSON := flag.Bool("json", false, "output as JSON")
	prettyPrint := flag.Bool("pretty-print", false, "append indents to the output JSON")
	jsonFirst := flag.Bool("json-first", false, "make JSON format preferred for input files")
	version := flag.Bool("version", false, "print the utility version")
	help := flag.Bool("help", false, "show this text")

	flag.Parse()

	if *outputJSON && *output == "out/api.yml" {
		*output = "out/api.json"
	}

	if *version {
		fmt.Printf("version: %s\n", Version)
		os.Exit(0)
	}

	if *help {
		flag.Usage()
		os.Exit(0)
	}

	var unmarshal parsers.Unmarshaller

	if *jsonFirst {
		unmarshal = parsers.UnmarshalJSONFirst
	} else {
		unmarshal = parsers.UnmarshalYAMLFirst
	}

	data, err := os.ReadFile(*basePath)
	checkf("Could not read file \"%s\"", err, *basePath)

	document := make(map[string]any)

	err = unmarshal(data, &document)
	checkf("Could not parse base data", err)

	if *tagsStr != "" {
		tags, err := parsers.List(*tagsStr, unmarshal)
		checkf("Could not parse tags section \"%s\"", err, *tagsStr)
		document["tags"] = tags
	}

	var components map[string]any

	if *schemasStr != "" {
		schemas, err := parsers.Map(*schemasStr, unmarshal)
		checkf("Could not parse schemas section \"%s\"", err, *schemasStr)
		components = make(map[string]any)
		components["schemas"] = schemas
	}

	if *pathsStr != "" {
		paths, err := parsers.Map(*pathsStr, unmarshal)
		checkf("Could not parse paths section \"%s\"", err, *pathsStr)
		document["paths"] = paths
	}

	if *securitySchemesStr != "" {
		securitySchemes, err := parsers.Map(*securitySchemesStr, unmarshal)
		checkf("Could not parse security schemes section \"%s\"", err, *securitySchemesStr)
		if components == nil {
			components = make(map[string]any)
		}
		components["securitySchemes"] = securitySchemes
	}

	if *responsesStr != "" {
		responses, err := parsers.Map(*responsesStr, unmarshal)
		checkf("Could not parse responses section \"%s\"", err, *responsesStr)
		if components == nil {
			components = make(map[string]any)
		}
		components["responses"] = responses
	}

	if components != nil {
		document["components"] = components
	}

	if *customStr != "" {
		custom, err := parsers.Custom(*customStr, unmarshal)
		checkf("Could not parse custom sections \"%s\"", err, *customStr)
		deepApply(document, custom)
		checkf("Could not apply custom sections \"%s\"", err, *customStr)
	}

	var outData []byte

	if *outputJSON {
		if *prettyPrint {
			outData, err = json.MarshalIndent(&document, "", "  ")
		} else {
			outData, err = json.Marshal(&document)
		}
	} else {
		outData, err = yaml.Marshal(&document)
	}

	checkf("Could not marshal the result", err)

	if *toStdout {
		fmt.Printf("%s", outData)
	} else {
		err = os.WriteFile(*output, outData, 0644)
		checkf("Could not write the output file \"%s\"", err, *output)
	}
}

func checkf(format string, err error, args ...any) {
	if err != nil {
		fmt.Fprintf(os.Stderr, format+", reason: %s\n", append(args[:], err.Error())...)
		os.Exit(1)
	}
}

func deepApply(document, sections map[string]any) error {
	for key, value := range sections {
		keyPath := strings.Split(key, "/")
		length := len(keyPath)
		d := document
		for i := 0; i < length; i++ {
			k := keyPath[i]
			sec, ok := d[k]
			if i == length-1 {
				if !ok { // undeclared
					d[k] = value
				} else {
					var err error
					d[k], err = parsers.Merge(sec, value)
					if err != nil {
						return fmt.Errorf("%s in \"%s\"", err.Error(), key)
					}
				}
			} else {
				if !ok {
					d[k] = make(map[string]any)
				}
				d = d[k].(map[string]any)
			}
		}
	}

	return nil
}
