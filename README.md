# docgen

`docgen` is an utility that builds an OpenAPI document from separated parts.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Golang](https://golang.org/)
* [Docker](https://www.docker.com) (optional)

## Installation

```sh
git clone <repository-url> # this repository
cd docgen
go mod vendor
```

## Running/Development

```sh
go run main.go [args]
```

## Building/Production

```sh
make clean && make
```

## Installation

```sh
sudo make install
```

## Docker usage

Build a docker image with the name `devoter/docgen`.

```sh
docker build --build-arg="GIT_COMMIT=$(git describe --tags --long --always)" -t devoter/docgen .
```

```sh
docker run --rm -i -t --mount type=bind,source="$(pwd)"/example,target=/example devoter/docgen /docgen [args]
```

## Command line arguments

* **schemas** - schemas files/directories (separated by comma, e.g. `--schemas=schemas/,../other/schemas/schemas.yml`);
* **security** - security definitions files/directories (separated by comma);
* **paths** - paths files/directories (separated by comma);
* **tags** - tags files/directories (separated by comma);
* **responses** - responses files (separated by comma);
* **custom** - [custom sections](#custom-sections);
* **output** (default: _out/api.yml_ / _out/api.json_) - output filename;
* **stdout** (default: _false_) - use `STDOUT`;
* **json** (default: _false_) - output as JSON;
* **json-first** (default: _false_) - make JSON format preferred for input files;
* **pretty-print** (default: _false_) - append indents to the output JSON;
* **version** - print the utility version;
* **help** - show command line help text.

### Custom sections

You may want to append your own custom sections or not prepared OpenAPI sections. So, you should use `custom` command line argument. Custom section includes three parts: _full section name_, _type_ and _path_. The **path** parameter is just a path to the file or directory. **Full section name** is a section path, separated by comma (e.g. `my/own/custom/section`). The **type** parameter have two valid values: `0` - `map`, and `1` - `list`. Parts are splitted by semicolon. Full example: `my/own/custom/section:0:example/custom.yml`.
Additionally you may set multiple sections separated by comma.

# License

[BSD 3-Clause](./LICENSE)