FROM golang:alpine AS builder

ARG GIT_COMMIT="undefined"

RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/src/gitlab.com/devoter/docgen/
COPY . .

RUN go mod vendor && \
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
        -ldflags="-X main.Version=${GIT_COMMIT} -w -s" \
        -o $GOPATH/bin/docgen main.go

FROM scratch
COPY --from=builder /go/bin/docgen /docgen
CMD ["/docgen"]
