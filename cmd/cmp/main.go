package main

import (
	"flag"
	"fmt"
	"os"
	"reflect"

	"gopkg.in/yaml.v3"
)

func main() {
	op1Path := flag.String("op1", "", "operator 1")
	op2Path := flag.String("op2", "", "operator 2")

	flag.Parse()

	op1Data, err := os.ReadFile(*op1Path)
	checkf("Could not read file \"%s\"", err, *op1Path)

	op2Data, err := os.ReadFile(*op2Path)
	checkf("Could not read file \"%s\"", err, *op2Path)

	op1 := make(map[string]any)
	err = yaml.Unmarshal(op1Data, &op1)
	checkf("Could not parse op1 data", err)

	op2 := make(map[string]any)
	err = yaml.Unmarshal(op2Data, &op2)
	checkf("Could not parse op2 data", err)

	if reflect.DeepEqual(op1, op2) {
		fmt.Printf("\"%s\" equals to \"%s\"\n", *op1Path, *op2Path)
	} else {
		fmt.Printf("\"%s\" not equals to \"%s\"\n", *op1Path, *op2Path)
	}
}

func checkf(format string, err error, args ...any) {
	if err != nil {
		fmt.Fprintf(os.Stderr, format+", reason: %s\n", append(args[:], err)...)
		os.Exit(1)
	}
}
