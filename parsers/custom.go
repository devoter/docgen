package parsers

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var nameRegex = regexp.MustCompile("^[0-9a-zA-Z_./]+$")

// Custom parses custom YAML/JSON sections.
// Each argument section should have three parts: section name, type (0 - map, 1 - list) and path.
// Paths exampe: `custom/section:0:custom/section.yml,custom/other:0:custom/other.yml,`
func Custom(paths string, unmarshal Unmarshaller) (map[string]any, error) {
	separated := strings.Split(paths, ",")
	sections := make(map[string]any)

	for _, v := range separated {
		if v == "" {
			continue
		}

		splitted := strings.Split(v, ":")
		if len(splitted) != 3 {
			return nil, fmt.Errorf("invalid argument format of \"%s\"", v)
		}

		name := splitted[0]
		if !nameRegex.MatchString(name) {
			return nil, fmt.Errorf("invalid section name format of \"%s\"", name)
		}

		sectionType, err := strconv.ParseUint(splitted[1], 2, 8)
		if err != nil {
			return nil, fmt.Errorf("section type of \"%s\" should be 0 or 1, but got \"%s\"", v, splitted[1])
		} else if sectionType != 0 && sectionType != 1 {
			return nil, fmt.Errorf("section type of \"%s\" should 0 or 1, but got \"%d\"", v, sectionType)
		}

		path := splitted[2]
		var section any

		if sectionType == 1 { // list
			section, err = List(path, unmarshal)
		} else { // map
			section, err = Map(path, unmarshal)
		}

		if err != nil {
			return nil, err
		}

		sec, ok := sections[name]
		if !ok {
			sections[name] = section
		} else {
			sections[name], err = Merge(sec, section)
			if err != nil {
				return nil, fmt.Errorf("%s of \"%s\"", err, v)
			}
		}
	}

	return sections, nil
}
