package parsers

import (
	"encoding/json"

	"github.com/tidwall/jsonc"
	"gopkg.in/yaml.v3"
)

type Unmarshaller func(data []byte, v any) error

// UnmarshalYAMLFirst attempts to parse the data as a YAML document and as a JSON document if that fails.
func UnmarshalYAMLFirst(data []byte, v any) error {
	if err := yaml.Unmarshal(data, v); err != nil {

		if e := json.Unmarshal(jsonc.ToJSON(data), v); e != nil {
			return err
		}
	}

	return nil
}

// UnmarshalJSONFirst attempts to parse the data as a JSON document and as a JSON document if that fails.
func UnmarshalJSONFirst(data []byte, v any) error {
	if err := json.Unmarshal(jsonc.ToJSON(data), v); err != nil {
		if e := yaml.Unmarshal(data, v); e != nil {
			return err
		}
	}

	return nil
}
