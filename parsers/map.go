package parsers

import "os"

// Map an OpenAPI section files.
func Map(path string, unmarshal Unmarshaller) (map[string]any, error) {
	paths, err := Paths(path)
	if err != nil {
		return nil, err
	}
	section := make(map[string]any)

	for _, item := range paths {
		if item == "" {
			continue
		}

		data, err := os.ReadFile(item)
		if err != nil {
			return nil, err
		}

		items := make(map[string]any)
		err = unmarshal(data, &items)
		if err != nil {
			return nil, err
		}

		section = MergeMap(section, items)
	}

	return section, nil
}
