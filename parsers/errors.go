package parsers

import "errors"

// ErrorIncompatibleSections means that two sections with the one name have different types (map and list).
var ErrorIncompatibleSections = errors.New("incompatible sections")

// ErrorUnexpectedSectionType means that the sections type is unknown (not `[]interface{}` or `map[string]interface{}`)
var ErrorUnexpectedSectionType = errors.New("unexpected section type")
