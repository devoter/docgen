package parsers

import "reflect"

// MergeList returns a new combined list.
func MergeList(dst, src []any) []any {
	for _, value := range src {
		found := false

		for _, v := range dst {
			if reflect.DeepEqual(value, v) {
				found = true
				break
			}
		}

		if !found {
			dst = append(dst, value)
		}
	}

	return dst
}

// MergeMap returns a new combined map.
func MergeMap(dst, src map[string]any) map[string]any {
	result := make(map[string]any)
	for key, value := range dst {
		result[key] = value
	}

	for key, value := range src {
		result[key] = value
	}

	return result
}

// Merge returns a combined resource with automatic casting.
func Merge(dst any, src any) (any, error) {
	if section, ok := dst.([]any); ok {
		items, ok := src.([]any)
		if !ok {
			return nil, ErrorIncompatibleSections
		}

		return MergeList(section, items), nil
	} else if section, ok := dst.(map[string]any); ok {
		items, ok := src.(map[string]any)
		if !ok {
			return nil, ErrorIncompatibleSections
		}

		return MergeMap(section, items), nil
	}

	return nil, ErrorUnexpectedSectionType
}
