package parsers

import "os"

// List parses OpenAPI sections with the list type.
func List(path string, unmarshal Unmarshaller) ([]any, error) {
	paths, err := Paths(path)
	if err != nil {
		return nil, err
	}
	section := make([]any, 0)

	for _, item := range paths {
		if item == "" {
			continue
		}

		data, err := os.ReadFile(item)
		if err != nil {
			return nil, err
		}

		var items []any
		err = unmarshal(data, &items)
		if err != nil {
			return nil, err
		}

		section = MergeList(section, items)
	}

	return section, nil
}
