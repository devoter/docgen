package parsers

import (
	"os"
	"regexp"
	"strings"
)

var pathRegex = regexp.MustCompile(`^.+\.(ya?ml|jsonc?)$`)

// Paths parses all paths in the list separated by comma and returns the strings list.
// If the path is a directory than the function adds all YAML/JSON files in the directory.
func Paths(paths string) ([]string, error) {
	separated := strings.Split(paths, ",")
	items := make([]string, 0)

	for _, path := range separated {
		file, err := os.Open(path)
		if err != nil {
			return nil, err
		}

		fi, err := file.Stat()
		if err != nil {
			file.Close()
			return nil, err
		} else if fi.IsDir() {
			files, err := os.ReadDir(path)
			if err != nil {
				file.Close()
				return nil, err
			}

			for _, f := range files {
				if pathRegex.MatchString(f.Name()) {
					items = append(items, path+"/"+f.Name())
				}
			}
		} else {
			items = append(items, path)
		}

		file.Close()
	}

	return items, nil
}
