NAME=docgen
GIT_COMMIT=$(shell git describe --tags --long --always --dirty)
GO_LDFLAGS="-X main.Version=$(GIT_COMMIT) -s -w"
PREFIX=/usr/local/bin

$(NAME):
	CGO_ENABLED=0 go build -ldflags $(GO_LDFLAGS) -o $(NAME) main.go

install: $(NAME)
	cp -v $(NAME) $(PREFIX)/

uninstall:
	rm -f $(PREFIX)/$(NAME)

clean:
	rm -f docgen